{
    "id": "de74a282-17c6-4f63-88a2-48ae360ef71f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f1046a5-dd5d-41be-a9be-4d2ddd314c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "d0ceb055-aada-476a-b851-9a1d0cbf935f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1046a5-dd5d-41be-a9be-4d2ddd314c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287fe1e1-fe1f-4d33-96ab-f9b61afe9bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1046a5-dd5d-41be-a9be-4d2ddd314c62",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "088b7614-3d6e-4274-89c1-fd96318505ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "15c36bcb-930d-4da1-b35b-fb07c15235b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088b7614-3d6e-4274-89c1-fd96318505ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09da975-e449-4ae9-b442-fac8c42ba315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088b7614-3d6e-4274-89c1-fd96318505ac",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "0d6b9c97-8113-4b5d-8b8f-b009f74a3ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "3447b9f7-73f7-4a82-9b20-3deb4528e33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6b9c97-8113-4b5d-8b8f-b009f74a3ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97145af-9fa9-47cd-9ac0-d855700f85bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6b9c97-8113-4b5d-8b8f-b009f74a3ca4",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "b0e41906-c00b-4253-b236-ab54de9128d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "a0be856b-6496-474e-80c9-7dbe612ae601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e41906-c00b-4253-b236-ab54de9128d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b803d3-9cc0-45df-b94f-46aef5408713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e41906-c00b-4253-b236-ab54de9128d2",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "43415da0-9855-41c1-8c87-78fe5e1d3baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "d1203cbf-94d9-4e69-bdd3-c787d0e5f787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43415da0-9855-41c1-8c87-78fe5e1d3baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6600ca8e-9ee3-4c3a-8cfb-e7e16cf69c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43415da0-9855-41c1-8c87-78fe5e1d3baf",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "92f3cd82-3765-4246-8099-890b174aaf4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "1bd7f5f3-3b83-41b4-9a11-35807329d52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f3cd82-3765-4246-8099-890b174aaf4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "122554b2-19d7-42ba-9484-03839e0f2e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f3cd82-3765-4246-8099-890b174aaf4a",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "f05d1700-805f-4768-9c00-df33961c28b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "746be499-552d-4e4f-b4ab-b7228e393eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05d1700-805f-4768-9c00-df33961c28b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b501bdc9-9ba3-40ba-9751-d499d3ecabe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05d1700-805f-4768-9c00-df33961c28b2",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        },
        {
            "id": "26ef73ec-9b36-4583-a5c0-50e102388032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "compositeImage": {
                "id": "80f194ea-3a74-4501-a2c6-69b3ac2afff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ef73ec-9b36-4583-a5c0-50e102388032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2fa3a8d-996e-4896-bfd0-a4d7c9a5304b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ef73ec-9b36-4583-a5c0-50e102388032",
                    "LayerId": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e5d2b4bc-42d4-4b5c-a5d8-5d29947472c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de74a282-17c6-4f63-88a2-48ae360ef71f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}